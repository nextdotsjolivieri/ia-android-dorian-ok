package com.nextdots.iteractive.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nextdots.iteractive.R;
import com.nextdots.iteractive.data.models.Complejo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ComplejosAdapter extends RecyclerView.Adapter<ComplejosAdapter.ComplejosViewHolder>{

    private List<Complejo> list;
    private Context context;

    public static class ComplejosViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvNameComplejo) TextView tvNameComplejo;
        @BindView(R.id.tvAddress) TextView tvAddress;

        ComplejosViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public ComplejosAdapter(Context context, List<Complejo> list){
        this.context = context;
        this.list = list;
    }

    public ComplejosAdapter(Context context) {
        this.context = context;
        this.list = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @Override
    public ComplejosAdapter.ComplejosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_complejo,parent,false);
        return new ComplejosViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ComplejosViewHolder holder, int position) {
        Complejo complejo = list.get(position);
        holder.tvNameComplejo.setText(complejo.nombre);
        holder.tvAddress.setText(complejo.direccion);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public Complejo getItemPosition(int position){
        return list.get(position);
    }

}