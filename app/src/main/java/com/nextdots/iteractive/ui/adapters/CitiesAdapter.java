package com.nextdots.iteractive.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nextdots.iteractive.R;
import com.nextdots.iteractive.data.models.City;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.CitiesViewHolder>{

    private List<City> list;
    private Context context;

    public static class CitiesViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvNameCity) TextView tvNameCity;

        CitiesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public CitiesAdapter(Context context, List<City> list){
        this.context = context;
        this.list = list;
    }

    public CitiesAdapter(Context context) {
        this.context = context;
        this.list = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @Override
    public CitiesAdapter.CitiesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city,parent,false);
        return new CitiesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CitiesViewHolder holder, int position) {
        City city = list.get(position);
        holder.tvNameCity.setText(city.Nombre);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public City getItemPosition(int position){
        return list.get(position);
    }


}