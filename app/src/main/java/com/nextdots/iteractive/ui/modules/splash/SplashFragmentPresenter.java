package com.nextdots.iteractive.ui.modules.splash;

import com.nextdots.iteractive.api.controllers.CityController;
import com.nextdots.iteractive.data.db.DBOperations;
import com.nextdots.iteractive.data.models.City;

import java.util.List;


public class SplashFragmentPresenter {

    private final SplashFragmentView splashFragmentView;
    private final CityController cityController;
    private final DBOperations dbOperations;

    public SplashFragmentPresenter(SplashFragmentView splasFragmentView, DBOperations dbOperations,
                                   CityController cityController) {
        this.splashFragmentView = splasFragmentView;
        this.dbOperations = dbOperations;
        this.cityController = cityController;
    }

    public void VerifyDb() {
        if(dbOperations.isTableCityEmpty()){
            //PETICION PARA IMPORTAR DATOS
            splashFragmentView.showMessageGetting();
            cityController.getCities().subscribe(this::onSuccess , this::onFailure);
        }else{
            splashFragmentView.gotoMainFragment();
        }
    }


    public void onSuccess(List<City> cities){
        dbOperations.addCities(cities);
        splashFragmentView.gotoMainFragment();
    }

    public void onFailure(Throwable throwable){
        splashFragmentView.onFailure();
    }
}
