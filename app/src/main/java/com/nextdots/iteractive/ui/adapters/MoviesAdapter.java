package com.nextdots.iteractive.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.RequestManager;
import com.nextdots.iteractive.R;
import com.nextdots.iteractive.data.models.Funcion;
import com.nextdots.iteractive.utils.Constant;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder> {
    private static final String TAG = MoviesAdapter.class.getSimpleName();
    private final RequestManager glide;

    private List<Funcion> list;
    private Context context;

    public static class MoviesViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvNameMovie) TextView tvNameMovie;
        @BindView(R.id.tvHorario) TextView tvHorario;
        @BindView(R.id.ivPoster)
        ImageView ivPoster;

        MoviesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public MoviesAdapter(Context context, RequestManager glide, ArrayList<Funcion> movies) {
        this.context = context;
        this.glide = glide;
        this.list = movies;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @Override
    public MoviesAdapter.MoviesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie,parent,false);
        return new MoviesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MoviesViewHolder holder, int position) {
        Funcion funcion = list.get(position);
        holder.tvNameMovie.setText(funcion.getMovie().getTitulo());
        holder.tvHorario.setText( TextUtils.join(", ", funcion.getHorarios()));
        if(funcion.getMultimedias().size() > 0){
            glide.load(Constant.URL_IMAGE+funcion.getMultimedias().get(0).getArchivo())
                    .error(R.drawable.ic_film)
                    .dontAnimate()
                    .placeholder(R.drawable.ic_film)
                    .into(holder.ivPoster);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public Funcion getItemPosition(int position){
        return list.get(position);
    }

}