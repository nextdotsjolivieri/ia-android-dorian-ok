package com.nextdots.iteractive.ui.modules.mapComplejo;


import android.Manifest;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nextdots.iteractive.R;
import com.nextdots.iteractive.data.models.City;
import com.nextdots.iteractive.data.models.Complejo;
import com.nextdots.iteractive.data.models.PermissionResult;
import com.nextdots.iteractive.ui.base.BaseFragment;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * A simple {@link BaseFragment} subclass.
 * Use the {@link MapComplejoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapComplejoFragment extends BaseFragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        MapComplejoView {
    private static final String TAG = MapComplejoFragment.class.getSimpleName();
    private static final String ARG_CITY = "city";
    private static final String ARG_COMPLEJOS = "complejos";
    private static final int REQUEST_LOCATION = 2;

    private City city;
    private GoogleMap map;
    private GoogleApiClient mGoogleApiClient;
    private ArrayList<Complejo> complejos;

    @BindView(R.id.fragment_map_mapview)
    MapView mapView;


    public MapComplejoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param city
     * @param complejoList Parameter 1.
     * @return A new instance of fragment MapComplejoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MapComplejoFragment newInstance(City city, ArrayList<Complejo> complejoList) {
        MapComplejoFragment fragment = new MapComplejoFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_CITY, city);
        args.putParcelableArrayList(ARG_COMPLEJOS, complejoList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            city = getArguments().getParcelable(ARG_CITY);
            complejos = getArguments().getParcelableArrayList(ARG_COMPLEJOS);
        }
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_map_complejo;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync((googleMap) -> {
            map = googleMap;
            map.getUiSettings().setMyLocationButtonEnabled(true);
            map.getUiSettings().setZoomControlsEnabled(true);
            map.getUiSettings().setZoomGesturesEnabled(true);

            if (hasPermissionsOrRequest(REQUEST_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
                MapsInitializer.initialize(getActivity());
                onDragMarkers();
            }

        });

    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }


    @Override
    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();

    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    private void onDragMarkers() {
        map.clear();
        for (Complejo complejo : complejos) {
            map.addMarker(new MarkerOptions()
                    .position(new LatLng(complejo.latitud, complejo.longitud))
                    .title(complejo.nombre)
                    .snippet("Dirección: "+complejo.getAddressShort())
            );
        }
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(city.Latitud, city.Longitud), 11));
    }


    @Override
    public void onRequestPermissionsResult(PermissionResult result) {
        if (REQUEST_LOCATION == result.getRequestCode()) {
            if (result.isAllGranted()) {
                MapsInitializer.initialize(getActivity());
                onDragMarkers();
            } else {
                simpleToast(R.string.permission_denied);
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

}
