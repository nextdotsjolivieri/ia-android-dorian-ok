package com.nextdots.iteractive.ui.modules.detailCity;


import android.Manifest;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nextdots.iteractive.R;
import com.nextdots.iteractive.api.controllers.CityController;
import com.nextdots.iteractive.data.models.City;
import com.nextdots.iteractive.data.models.Complejo;
import com.nextdots.iteractive.data.models.PermissionResult;
import com.nextdots.iteractive.ui.adapters.DetailCityPagerAdapter;
import com.nextdots.iteractive.ui.base.BaseFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetailCityFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailCityFragment extends BaseFragment implements DetailCityView {

    private static final String ARG_CITY = "city";
    private static final int REQUEST_STORE = 1;

    @Inject
    CityController cityController;

    private DetailCityPagerAdapter mAdapter;

    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.sliding_tabs)
    TabLayout mViewTabs;

    private DetailCityPresenter detailCityPresenter;
    private City city;


    public DetailCityFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param city Parameter 1.
     * @return A new instance of fragment DetailCityFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailCityFragment newInstance(City city) {
        DetailCityFragment fragment = new DetailCityFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_CITY, city);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            city = getArguments().getParcelable(ARG_CITY);
        }
        initialize();
        setHaveToolbar(true);
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_detail_city;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        setTitle(city.Nombre);
        if (hasPermissionsOrRequest(REQUEST_STORE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE)) {
            detailCityPresenter.checkData(String.valueOf(city.Id));
        }
    }

    @Override
    public void onRequestPermissionsResult(PermissionResult result) {
        if (REQUEST_STORE == result.getRequestCode()) {
            if (result.isAllGranted()) {
                detailCityPresenter.checkData(String.valueOf(city.Id));
            } else {
                simpleToast(R.string.permission_denied);
            }
        }
    }

    private void initialize(){
        getApplicationComponent().inject(this);
        detailCityPresenter = new DetailCityPresenter(getContext(), this, cityController);
    }

    @Override
    public void failedDownload() {
        simpleToast(R.string.error_download_file);
    }

    @Override
    public void dismissProgress() {
        dismissDialog();
    }

    @Override
    public void showMessageLoading() {
        showProgressDialog();
    }

    @Override
    public void setupPagerAdapter(ArrayList<Complejo> complejos) {
        mAdapter = new DetailCityPagerAdapter(getChildFragmentManager(), getContext(), city, complejos);
        pager.setAdapter(mAdapter);
        mViewTabs.setupWithViewPager(pager);
    }

    @Override
    public void failedDB() {
        simpleToast(R.string.error_file);
        goBack();
    }
}
