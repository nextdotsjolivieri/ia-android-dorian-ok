package com.nextdots.iteractive.ui.modules.splash;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nextdots.iteractive.R;
import com.nextdots.iteractive.api.controllers.CityController;
import com.nextdots.iteractive.data.db.DBOperations;
import com.nextdots.iteractive.ui.base.BaseFragment;
import com.nextdots.iteractive.ui.modules.main.MainActivityView;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * A simple {@link BaseFragment} subclass.
 * Use the {@link SplashFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SplashFragment extends BaseFragment implements SplashFragmentView{
    private static final String TAG = SplashFragment.class.getSimpleName();

    @Inject
    DBOperations dbOperations;
    @Inject
    CityController cityController;

    @BindView(R.id.tvMessage)
    TextView tvMessage;

    private SplashFragmentPresenter splashFragmentPresenter;
    private MainActivityView mainActivityView;

    public SplashFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SplashFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SplashFragment newInstance() {
        return new SplashFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize();
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_splash;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        splashFragmentPresenter.VerifyDb();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivityView = (MainActivityView) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainActivityView = null;
    }

    private void initialize(){
        getApplicationComponent().inject(this);
        splashFragmentPresenter = new SplashFragmentPresenter(this , dbOperations, cityController);
    }

    //LISTENERS----------------------
    @Override
    public void gotoMainFragment() {
        if(mainActivityView != null){
            mainActivityView.gotoMain();
        }
    }

    @Override
    public void showMessageGetting() {
        tvMessage.setVisibility(View.VISIBLE);
        tvMessage.setText(R.string.getting_data);
    }

    @Override
    public void onFailure() {
        tvMessage.setText(R.string.error_getting_data);
    }
}
