package com.nextdots.iteractive.ui.modules.detailCity;

import com.nextdots.iteractive.data.models.Complejo;

import java.util.ArrayList;


public interface DetailCityView {

    void failedDownload();

    void dismissProgress();

    void showMessageLoading();

    void setupPagerAdapter(ArrayList<Complejo> complejos);

    void failedDB();
}
