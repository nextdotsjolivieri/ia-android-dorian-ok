package com.nextdots.iteractive.ui.modules.detailComplejo;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.nextdots.iteractive.R;
import com.nextdots.iteractive.data.models.City;
import com.nextdots.iteractive.data.models.Complejo;
import com.nextdots.iteractive.data.models.Funcion;
import com.nextdots.iteractive.ui.adapters.MoviesAdapter;
import com.nextdots.iteractive.ui.base.BaseFragment;
import com.nextdots.iteractive.ui.modules.detailMovie.DetailMovieFragment;
import com.nextdots.iteractive.utils.RecyclerItemClickListener;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * A simple {@link com.nextdots.iteractive.ui.base.BaseFragment} subclass.
 * Use the {@link DetailComplejoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailComplejoFragment extends BaseFragment implements DetailComplejoView{
    private static final String TAG = DetailComplejoFragment.class.getSimpleName();
    private static final String ARG_COMPLEJO = "complejo";
    private static final String ARG_CITY = "city";

    private DetailComplejoPresenter detailComplejoPresenter;
    private StaggeredGridLayoutManager mLayoutManager;
    private MoviesAdapter mAdapter;
    private ArrayList<Funcion> movies;
    private Complejo complejo;
    private City city;


    @BindView(R.id.rcMovies)
    RecyclerView rcMovies;


    public DetailComplejoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param complejo Parameter 1.
     * @return A new instance of fragment DetailComplejoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailComplejoFragment newInstance(City city, Complejo complejo) {
        DetailComplejoFragment fragment = new DetailComplejoFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_COMPLEJO, complejo);
        args.putParcelable(ARG_CITY, city);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            complejo = getArguments().getParcelable(ARG_COMPLEJO);
            city = getArguments().getParcelable(ARG_CITY);
        }
        initialize();
        setHaveToolbar(true);
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_complejo_detail;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        setTitle(R.string.today);
        showProgressDialog();
        detailComplejoPresenter.getMovies(city,complejo);
    }

    private void initialize(){
        getApplicationComponent().inject(this);
        detailComplejoPresenter = new DetailComplejoPresenter(getContext(), this);
    }

    @Override
    public void setupMoviesAdapter(ArrayList<Funcion> movies) {
        dismissDialog();
        this.movies = movies;

        if (movies.size() == 0) {
            simpleToast(R.string.empty_funciones);
        } else {
            mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
            rcMovies.setLayoutManager(mLayoutManager);
            mAdapter = new MoviesAdapter(getContext(), Glide.with(this), movies);

            rcMovies.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), (view, position) -> {
                   pushFragment(DetailMovieFragment.newInstance(city, complejo, movies.get(position)));
            }));
            rcMovies.setAdapter(mAdapter);
        }
    }
}
