package com.nextdots.iteractive.ui.modules.splash;


public interface SplashFragmentView {
    void gotoMainFragment();
    void showMessageGetting();
    void onFailure();
}
