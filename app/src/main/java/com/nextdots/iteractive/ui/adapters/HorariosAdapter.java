package com.nextdots.iteractive.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nextdots.iteractive.R;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dars_ on 2/5/2017.
 */

public class HorariosAdapter  extends RecyclerView.Adapter<HorariosAdapter.HoariosViewHolder>{

    private List<String> list;
    private Context context;

    public static class HoariosViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvHorario)
        TextView tvHorario;

        HoariosViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public HorariosAdapter(Context context, List<String> list){
        this.context = context;
        this.list = list;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @Override
    public HorariosAdapter.HoariosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_horario,parent,false);
        return new HorariosAdapter.HoariosViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HorariosAdapter.HoariosViewHolder holder, int position) {
        holder.tvHorario.setText(context.getResources().getString(R.string.funcion_para,list.get(position)));
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public String getItemPosition(int position){
        return list.get(position);
    }



}
