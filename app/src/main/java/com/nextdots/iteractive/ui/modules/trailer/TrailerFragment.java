package com.nextdots.iteractive.ui.modules.trailer;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.nextdots.iteractive.R;
import com.nextdots.iteractive.data.models.Funcion;
import com.nextdots.iteractive.ui.base.BaseFragment;
import com.nextdots.iteractive.utils.Constant;

import butterknife.BindView;

/**
 * A simple {@link BaseFragment} subclass.
 * Use the {@link TrailerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TrailerFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_FUNCION = "funcion";
    private static final String TAG = TrailerFragment.class.getSimpleName();

    private MediaController mediacontroller;
    private Funcion funcion;

    @BindView(R.id.videoView)
    VideoView videoView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.llError)
    LinearLayout llError;

    public TrailerFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param funcion Parameter 1.
     * @return A new instance of fragment TrailerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TrailerFragment newInstance(Funcion funcion) {
        TrailerFragment fragment = new TrailerFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_FUNCION, funcion);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            funcion = getArguments().getParcelable(ARG_FUNCION);
        }
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_trailer;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        startVideo();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(videoView.isPlaying()){
            mediacontroller.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(videoView.isPlaying()){
            mediacontroller.setVisibility(View.GONE);
        }
    }

    private void startVideo() {
        progressBar.setVisibility(View.VISIBLE);
        try {
            mediacontroller = new MediaController(
                    getActivity());
            mediacontroller.setAnchorView(videoView);
            Uri video = Uri.parse(Constant.URL_VIDEO + funcion.getId_pelicula() +"/1/2/" +
                    funcion.getId_pelicula() + ".webm");
            videoView.setMediaController(mediacontroller);
            videoView.setVideoURI(video);

        } catch (Exception e) {
            Log.e(TAG+" Dars", e.getMessage());
            e.printStackTrace();
            progressBar.setVisibility(View.GONE);
            videoView.setVisibility(View.GONE);
            llError.setVisibility(View.VISIBLE);
        }

        videoView.requestFocus();
        videoView.setOnErrorListener( ( mp, what, extra) -> {
                progressBar.setVisibility(View.GONE);
                videoView.setVisibility(View.GONE);
                llError.setVisibility(View.VISIBLE);
                return false;
        });
        videoView.setOnPreparedListener((mp) -> {
            videoView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            videoView.start();
        });

    }
}
