package com.nextdots.iteractive.ui.modules.main;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nextdots.iteractive.R;
import com.nextdots.iteractive.api.controllers.CityController;
import com.nextdots.iteractive.data.db.DBOperations;
import com.nextdots.iteractive.data.models.City;
import com.nextdots.iteractive.ui.adapters.CitiesAdapter;
import com.nextdots.iteractive.ui.base.BaseFragment;
import com.nextdots.iteractive.ui.modules.detailCity.DetailCityFragment;
import com.nextdots.iteractive.utils.RecyclerItemClickListener;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * A simple {@link com.nextdots.iteractive.ui.base.BaseFragment} subclass.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFragment extends BaseFragment implements MainFragmentView {
    //CONSTANTS
    private static final String TAG = MainFragment.class.getSimpleName();

    @Inject
    DBOperations dbOperations;

    @BindView(R.id.rcCities)
    RecyclerView rcCities;

    private MainFragmentPresenter mainFragmentPresenter;
    private CitiesAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<City> cities;


    public MainFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MainFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize();
        setHaveToolbar(true);
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_main;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        setTitle(R.string.cities);
        setupImageToolbar(R.drawable.ic_launch, false);
        showProgressDialog();
        mainFragmentPresenter.getCities();
    }

    private void initialize() {
        getApplicationComponent().inject(this);
        mainFragmentPresenter = new MainFragmentPresenter(this, dbOperations);
    }

    //LISTENERS---------------------------------
    @Override
    public void setCitiesAdapter(List<City> cities) {
        dismissDialog();
        this.cities = cities;
        if (cities.size() == 0) {
            simpleToast(R.string.empty_cities);
        } else {
            mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
            rcCities.setLayoutManager(mLayoutManager);
            mAdapter = new CitiesAdapter(getContext(), cities);

            rcCities.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), (view, position) -> {
                pushFragment(DetailCityFragment.newInstance(cities.get(position)));
            }));
            rcCities.setAdapter(mAdapter);
        }
    }

}
