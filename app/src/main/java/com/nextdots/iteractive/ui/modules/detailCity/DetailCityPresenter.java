package com.nextdots.iteractive.ui.modules.detailCity;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import com.nextdots.iteractive.api.controllers.CityController;
import com.nextdots.iteractive.data.models.Complejo;
import com.nextdots.iteractive.utils.DownloadUtils;

import java.io.File;
import java.util.ArrayList;



public class DetailCityPresenter {
    private static final String TAG = DetailCityPresenter.class.getSimpleName();

    private final DetailCityView detailCityView;
    private final CityController cityController;
    private final Context context;
    private long downloadReference;
    private DownloadManager downloadmanager;
    private String ciudad_id;

    public DetailCityPresenter(Context context, DetailCityView detailCityView, CityController cityController) {
        this.detailCityView = detailCityView;
        this.cityController = cityController;
        this.context = context;

        //set filter to only when download is complete and register broadcast receiver
        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        context.getApplicationContext().registerReceiver(downloadReceiver, filter);
    }

    public void checkData(String ciudad_id) {
        this.ciudad_id = ciudad_id;
        File file = new File(Environment.getExternalStorageDirectory() + "/Download/", "db_"+ciudad_id + ".sqlite");
        detailCityView.showMessageLoading();
        if (file.exists()) {
            getDataSqlite(file.getPath());
        } else {
            //NO EXISTE EL ARCHIVO
            downloadReference = DownloadUtils.startDownload(context, downloadmanager, ciudad_id);
        }
    }

    private void getDataSqlite(String path) {
        SQLiteDatabase db;
        try{
            db = SQLiteDatabase.openDatabase(path, null, 0);
        }catch (Exception e){
            detailCityView.failedDB();
            return;
        }

        Cursor cursor = db.query("Complejo", null, null, null, null, null, null);
        ArrayList<Complejo> complejos = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Complejo complejo = new Complejo();
                complejo.id = cursor.getInt(0);
                complejo.id_complejo_vista = cursor.getString(1);
                complejo.id_ciudad = cursor.getInt(2);
                complejo.nombre = cursor.getString(3);
                complejo.latitud = cursor.getDouble(4);
                complejo.longitud = cursor.getDouble(5);
                complejo.telefono = cursor.getString(6);
                complejo.direccion = cursor.getString(7);
                complejo.es_compra_anticipada = cursor.getInt(8) == 1;
                complejo.es_reserva_permitida = cursor.getInt(9) == 1;
                complejo.url_sitio = cursor.getString(10);
                complejos.add(complejo);
            } while (cursor.moveToNext());
        }

        detailCityView.setupPagerAdapter(complejos);
        detailCityView.dismissProgress();
    }

    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //check if the broadcast message is for our Enqueued download
            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            if (downloadReference == referenceId) {
                try {
                    File file = new File(Environment.getExternalStorageDirectory() + "/Download/", "db_"+ciudad_id + ".sqlite");
                    getDataSqlite(file.getPath());
                } catch (Exception e) {
                    detailCityView.dismissProgress();
                    detailCityView.failedDownload();
                }
            }
        }
    };
}
