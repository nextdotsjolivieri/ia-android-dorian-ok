package com.nextdots.iteractive.ui.modules.main;


import com.nextdots.iteractive.data.models.City;

import java.util.List;


public interface MainFragmentView {
    void setCitiesAdapter(List<City> cities);
}
