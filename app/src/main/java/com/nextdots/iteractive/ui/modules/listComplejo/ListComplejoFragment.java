package com.nextdots.iteractive.ui.modules.listComplejo;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nextdots.iteractive.R;
import com.nextdots.iteractive.data.models.City;
import com.nextdots.iteractive.data.models.Complejo;
import com.nextdots.iteractive.ui.adapters.ComplejosAdapter;
import com.nextdots.iteractive.ui.base.BaseFragment;
import com.nextdots.iteractive.ui.modules.detailComplejo.DetailComplejoFragment;
import com.nextdots.iteractive.utils.RecyclerItemClickListener;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * A simple {@link BaseFragment} subclass.
 * Use the {@link ListComplejoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListComplejoFragment extends BaseFragment implements ListComplejoView{
    private static final String ARG_COMPLEJOS = "complejos";
    private static final String ARG_CITY = "city";

    private StaggeredGridLayoutManager mLayoutManager;
    private ArrayList<Complejo> complejos;
    private ComplejosAdapter mAdapter;
    private City city;

    @BindView(R.id.rcComplejos)
    RecyclerView rcComplejos;



    public ListComplejoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     *
     * @param city
     * @param complejoList Parameter 1.
     * @return A new instance of fragment ListComplejoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ListComplejoFragment newInstance(City city, ArrayList<Complejo> complejoList) {
        ListComplejoFragment fragment = new ListComplejoFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_COMPLEJOS, complejoList);
        args.putParcelable(ARG_CITY, city);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            complejos = getArguments().getParcelableArrayList(ARG_COMPLEJOS);
            city = getArguments().getParcelable(ARG_CITY);
        }
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_list_complejo;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        setComplejosAdapter(complejos);
    }


    @Override
    public void setComplejosAdapter(ArrayList<Complejo> complejos) {
        dismissDialog();
        this.complejos = complejos;
        if (complejos.size() == 0) {
            simpleToast(R.string.empty_complejos);
        } else {
            mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
            rcComplejos.setLayoutManager(mLayoutManager);
            mAdapter = new ComplejosAdapter(getContext(), complejos);

            rcComplejos.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), (view, position) -> {
                pushFragment(DetailComplejoFragment.newInstance(city, complejos.get(position)));
            }));
            rcComplejos.setAdapter(mAdapter);
        }
    }
}
