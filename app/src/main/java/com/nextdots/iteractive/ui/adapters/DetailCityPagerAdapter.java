package com.nextdots.iteractive.ui.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.nextdots.iteractive.R;
import com.nextdots.iteractive.data.models.City;
import com.nextdots.iteractive.data.models.Complejo;
import com.nextdots.iteractive.ui.modules.listComplejo.ListComplejoFragment;
import com.nextdots.iteractive.ui.modules.mapComplejo.MapComplejoFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



public class DetailCityPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 2;
    private Context context;
    private final City city;
    private FragmentManager fm;
    private ArrayList<Complejo> complejos;
    private Map<Integer, String> mFragmentTags;
    private int[] stringResId = {
        R.string.list, R.string.map
    };


    public DetailCityPagerAdapter(FragmentManager fm, Context context, City city, ArrayList<Complejo> complejos) {
        super(fm);
        this.fm = fm;
        this.context = context;
        this.city = city;
        this.complejos = complejos;
        mFragmentTags = new HashMap<>();
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0) {
            return ListComplejoFragment.newInstance(city,complejos);
        }else{
            return MapComplejoFragment.newInstance(city,complejos);
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return context.getResources().getString(stringResId[position]);
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object obj = super.instantiateItem(container, position);
        if (obj instanceof Fragment) {
            // record the fragment tag here.
            Fragment f = (Fragment) obj;
            String tag = f.getTag();
            mFragmentTags.put(position, tag);
        }
        return obj;
    }

    public Fragment getFragment(int position) {
        String tag = mFragmentTags.get(position);
        if (tag == null)
            return null;
        return fm.findFragmentByTag(tag);
    }


}
