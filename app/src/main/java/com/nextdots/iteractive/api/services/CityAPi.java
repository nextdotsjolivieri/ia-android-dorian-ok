package com.nextdots.iteractive.api.services;

import com.nextdots.iteractive.data.models.City;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by dars_ on 1/5/2017.
 */

public interface CityAPi {
    @GET("Consumo.svc/json/ObtenerCiudades")
    Observable<List<City>> getCities();
}
