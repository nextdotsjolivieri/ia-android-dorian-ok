package com.nextdots.iteractive.api.controllers;

import com.nextdots.iteractive.api.services.CityAPi;
import com.nextdots.iteractive.data.models.City;

import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class CityController {
    private final CityAPi cityApi;

    public CityController(CityAPi cityAPi) {
        this.cityApi = cityAPi;
    }

    public Observable<List<City>> getCities() {
        return cityApi.getCities().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
