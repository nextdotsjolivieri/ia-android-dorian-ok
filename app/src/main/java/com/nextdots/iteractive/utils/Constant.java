package com.nextdots.iteractive.utils;



public interface Constant {
    /*
    *
    *  SHA1: 68:DA:71:D9:8E:47:60:2C:38:86:86:0A:B1:74:BD:AC:E7:76:AE:A5
    *
    *
    * */

    String URL_VIDEO = "http://videos.cinepolis.com/";
    String URL_SERVER = "http://api.cinepolis.com.mx/";
    String URL_IMAGE = "http://www.cinepolis.com/_MOVIL/Android/galeria/thumb/";


    String DOWNLOAD = "cinespolis";
    String TABLE_CARTELERA = "Cartelera";
    String TABLE_PELICULA = "Pelicula";
    String TABLE_MULTIMEDIA = "Multimedia";
}
