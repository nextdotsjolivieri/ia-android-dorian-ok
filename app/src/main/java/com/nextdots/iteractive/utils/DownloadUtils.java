package com.nextdots.iteractive.utils;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;

import com.nextdots.iteractive.R;



public class DownloadUtils {


    public static Long startDownload(Context context, DownloadManager downloadmanager, String ciudad_id){
        String servicestring = Context.DOWNLOAD_SERVICE;
        downloadmanager = (DownloadManager) context.getSystemService(servicestring);
        Uri uri = Uri.parse(Constant.URL_SERVER + "sqlite.aspx?idCiudad=" + ciudad_id);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setTitle(context.getResources().getString(R.string.downloading));
        request.setDestinationInExternalPublicDir(
                Environment.DIRECTORY_DOWNLOADS,
                "db_"+ciudad_id + ".sqlite");

        return downloadmanager.enqueue(request);
    }
}
