package com.nextdots.iteractive.data.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.nextdots.iteractive.data.models.City;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;



public class DBOperations {
    private static final String TAG = DBOperations.class.getSimpleName();
    private DBHelper dbHelper;

    public DBOperations(Context context) {
        dbHelper = new DBHelper(context);
    }


    public Boolean isTableCityEmpty() {
        SQLiteDatabase dataBase = dbHelper.getReadableDatabase();
        Cursor cursor = dataBase.query(DBHelper.TABLE_CITY, null, null, null, null, null, null);
        return !cursor.moveToFirst();
    }

    public void addCities(List<City> cities) {
        SQLiteDatabase dataBase = dbHelper.getWritableDatabase();
        try{
            for (City city : cities){
                ContentValues values = new ContentValues();
                values.put(DBHelper.C_ESTADO, city.Estado);
                values.put(DBHelper.C_ID, city.Id);
                values.put(DBHelper.C_ID_ESTADO, city.IdEstado);
                values.put(DBHelper.C_ID_PAIS, city.IdPais);
                values.put(DBHelper.C_LATITUD, city.Latitud);
                values.put(DBHelper.C_LONGITUD, city.Longitud);
                values.put(DBHelper.C_NOMBRE, city.Nombre);
                values.put(DBHelper.C_PAIS, city.Pais);
                dataBase.insertWithOnConflict(DBHelper.TABLE_CITY, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            }
        }finally{
            dataBase.close();
        }
    }

    public Observable<List<City>> getCities(){
        SQLiteDatabase dataBase = dbHelper.getReadableDatabase();
        Cursor cursor = dataBase.query(DBHelper.TABLE_CITY, null, null, null, null, null, null);
        List<City> cities = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                City  city = new City();
                city.Estado = cursor.getString(DBHelper.C_ESTADO_INDEX);
                city.Id = cursor.getInt(DBHelper.C_ID_INDEX);
                city.IdEstado = cursor.getInt(DBHelper.C_ID_ESTADO_INDEX);
                city.IdPais = cursor.getInt(DBHelper.C_ID_PAIS_INDEX);
                city.Latitud = cursor.getDouble(DBHelper.C_LATITUD_INDEX);
                city.Longitud = cursor.getDouble(DBHelper.C_LONGITUD_INDEX);
                city.Nombre = cursor.getString(DBHelper.C_NOMBRE_INDEX);
                city.Pais = cursor.getString(DBHelper.C_PAIS_INDEX);
                cities.add(city);
            } while(cursor.moveToNext());
        }

        return Observable.create(subscriber -> {
            if (!subscriber.isUnsubscribed()) {
                subscriber.onNext(cities);
            }
        });
    }
}
