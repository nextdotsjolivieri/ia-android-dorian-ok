package com.nextdots.iteractive.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Funcion implements Parcelable{
    private int id_pelicula;
    private String id_complejo_vista;
    private int id_complejo;
    private String id_pelicula_vista;
    private int id_showtime;
    private String Fecha;
    private String Sello;
    private int Sala;
    private int Id;
    private Movie movie;
    private ArrayList<Multimedia> multimedias;
    private ArrayList<String> horarios;


    public Funcion() {

    }

    public Funcion(int id_pelicula, String id_complejo_vista, int id_complejo, String id_pelicula_vista, int id_showtime, String horario, String fecha, String sello, int sala, int id) {
        this.id_pelicula = id_pelicula;
        this.id_complejo_vista = id_complejo_vista;
        this.id_complejo = id_complejo;
        this.id_pelicula_vista = id_pelicula_vista;
        this.id_showtime = id_showtime;
        Fecha = fecha;
        Sello = sello;
        Sala = sala;
        Id = id;
        this.horarios = new ArrayList<>();
        horarios.add(horario);
    }

    public final static Parcelable.Creator<Funcion> CREATOR = new Creator<Funcion>() {

        public Funcion createFromParcel(Parcel in) {
            Funcion instance = new Funcion();
            instance.id_pelicula = in.readInt();
            instance.id_complejo_vista = in.readString();
            instance.id_complejo = in.readInt();
            instance.id_pelicula_vista = in.readString();
            instance.id_showtime =  in.readInt();
            instance.Fecha =  in.readString();
            instance.Sello = in.readString();
            instance.Id = in.readInt();
            instance.multimedias =  in.readArrayList(Multimedia.class.getClassLoader());
            instance.horarios = in.readArrayList(String.class.getClassLoader());
            return instance;
        }

        public Funcion[] newArray(int size) {
            return (new Funcion[size]);
        }

    };


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id_pelicula);
        dest.writeValue(id_complejo_vista);
        dest.writeValue(id_complejo);
        dest.writeValue(id_pelicula_vista);
        dest.writeValue(id_showtime);
        dest.writeValue(Fecha);
        dest.writeValue(Sello);
        dest.writeValue(Id);
        dest.writeValue(multimedias);
        dest.writeValue(horarios);
    }

    public int describeContents() {
        return 0;
    }

    public int getId_pelicula() {
        return id_pelicula;
    }

    public void setId_pelicula(int id_pelicula) {
        this.id_pelicula = id_pelicula;
    }

    public String getId_complejo_vista() {
        return id_complejo_vista;
    }

    public void setId_complejo_vista(String id_complejo_vista) {
        this.id_complejo_vista = id_complejo_vista;
    }

    public int getId_complejo() {
        return id_complejo;
    }

    public void setId_complejo(int id_complejo) {
        this.id_complejo = id_complejo;
    }

    public String getId_pelicula_vista() {
        return id_pelicula_vista;
    }

    public void setId_pelicula_vista(String id_pelicula_vista) {
        this.id_pelicula_vista = id_pelicula_vista;
    }

    public int getId_showtime() {
        return id_showtime;
    }

    public void setId_showtime(int id_showtime) {
        this.id_showtime = id_showtime;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String fecha) {
        Fecha = fecha;
    }

    public String getSello() {
        return Sello;
    }

    public void setSello(String sello) {
        Sello = sello;
    }

    public int getSala() {
        return Sala;
    }

    public void setSala(int sala) {
        Sala = sala;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public void setMultimedias(ArrayList<Multimedia> multimedia) {
        this.multimedias = multimedia;
    }

    public ArrayList<Multimedia> getMultimedias() {
        return multimedias;
    }

    public ArrayList<String> getHorarios() {
        return horarios;
    }

    public void setHorarios(ArrayList<String> horarios) {
        this.horarios = horarios;
    }

    public void addHorario(String horario) {
        horarios.add(horario);
    }

}
