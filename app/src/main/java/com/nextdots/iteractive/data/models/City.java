package com.nextdots.iteractive.data.models;

import android.os.Parcel;
import android.os.Parcelable;

public class City implements Parcelable{
    public String Estado;
    public int Id;
    public int IdEstado;
    public int IdPais;
    public double Latitud;
    public double Longitud;
    public String Nombre;
    public String Pais;

    public final static Parcelable.Creator<City> CREATOR = new Creator<City>() {

        public City createFromParcel(Parcel in) {
            City instance = new City();
            instance.Estado = in.readString();
            instance.Id = in.readInt();
            instance.IdEstado = in.readInt();
            instance.IdPais = in.readInt();
            instance.Latitud =  in.readDouble();
            instance.Longitud =  in.readDouble();
            instance.Nombre = in.readString();
            return instance;
        }

        public City[] newArray(int size) {
            return (new City[size]);
        }

    };

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(Estado);
        dest.writeValue(Id);
        dest.writeValue(IdEstado);
        dest.writeValue(IdPais);
        dest.writeValue(Latitud);
        dest.writeValue(Longitud);
        dest.writeValue(Nombre);
    }

    public int describeContents() {
        return 0;
    }
}
